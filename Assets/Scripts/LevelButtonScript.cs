﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelButtonScript : MonoBehaviour {

	public int levelIndex;
	public Image levelScoreIcon;
	public Sprite[] scoreIcons;
	public int score = 0;

	public Image LevelScreenShot;
	public Sprite[] levelScreenShots;

	public void SetScore (int i)
	{
		score = i;
		if (score >= 0) {
			levelScoreIcon.color = Color.white;
			levelScoreIcon.sprite = scoreIcons [score];
		}

		LevelScreenShot.sprite = levelScreenShots[levelIndex-1];
	}

	public void LoadLevel()
	{
		if (Application.loadedLevelName == "Menu") 
		{
			Camera.main.GetComponent<MainMenu> ().LevelButtonPressed (levelIndex);
		}
		else
		{
			Application.LoadLevel(levelIndex);
		}
	}

}
