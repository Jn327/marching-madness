﻿using UnityEngine;
using System.Collections;

public class VictoryTrigger : MonoBehaviour 
{
	public GameGUI gameGui;
	public Animation shopOwner;
	// Use this for initialization
	void Start ()
	{
		gameGui = Camera.main.transform.GetComponent<GameGUI> ();
	}

	void Update()
	{
		if (!shopOwner.isPlaying)
		{
			shopOwner.Play("Idle");
		}
	}


	void OnTriggerEnter (Collider other)
	{
		if (other.tag == "Minion")
		{
			MinionControls minionCont = other.GetComponent<MinionControls>();
			if (!minionCont.victory)
			{
				gameGui.minionsSaved ++;
				gameGui.minions.Remove(other.gameObject);

				//maybe replace with an anim???
				minionCont.victory = true;
				//Destroy(other.gameObject);
				shopOwner.Play("Agro");
			}
		}
	}
}
