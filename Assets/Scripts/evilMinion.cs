﻿using UnityEngine;
using System.Collections;

public class evilMinion : MonoBehaviour {

	CharacterController charController;
	public Animation anim;
	public float Move_Speed = 5;
	public float gravity = 50;
	private Vector3 moveDirection = Vector3.zero;
	public float stopDist = 2;
	public float stopDist_minion;
	public float health = 100;

	public GameObject destroy_Effect;


	float fallDmgToApply = 0;
	float lastGroundedHeight = 0;
	public float fallDMGHeightStart = 5;
	public float fallDMGMultip = 2;

	public float impatience_timer;
	public float impatience_Time = 3;
	public Renderer[] rend;

	
	public float dropDMGStart = 5f;
	
	public GameObject raycast_obj;
	public GameObject raycast_obj1;
	
	bool hitMinion = false;
	
	int jumptofall = 0;
	
	public Rigidbody [] body_parts;
	
	public AudioSource[] DieSound;
	bool dieSoundPlayed = false;

	public GameObject[] footSteps;

	public AudioSource[] jumpsounds;
	float timeLastJumpSoundPlayed = 0;

	public AudioSource[] attackSounds;

	
	// Use this for initialization
	void Start () 
	{
		charController = GetComponent<CharacterController> ();

		if (footSteps.Length > 0) {
			int randIndex = Random.Range (0, footSteps.Length);
			footSteps[randIndex].SetActive(true);
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (health <= 0) 
		{
			if (!dieSoundPlayed)
			{
				int randIndex = Random.Range(0,DieSound.Length);
				DieSound[randIndex].Play ();
				dieSoundPlayed = true;
			}
			for (int i = 0; i < body_parts.Length; i++)
			{
				if (body_parts[i] != null)
				{
					anim.Stop ();
					body_parts [i].isKinematic = false;
					body_parts [i].useGravity = true;
				}
			}
			charController.enabled = false;
			StartCoroutine("DestroyTime",5);
		} 
		else 
		{
			//change color if impatient....
			if (impatience_timer >= impatience_Time - 1.5f) {
				float ping = Mathf.PingPong (impatience_timer*3, 1);
				/*float speed = 1 - (impatience_timer / impatience_Time);
				ping = (Mathf.Sin (Time.time * speed) + 1.0f) / 2.0f;*/
				Color lerpedColor = Color.Lerp (Color.white, Color.red, ping);
				for (int i = 0; i < rend.Length; i++) {
					if (rend[i] != null)
					{
						rend [i].material.color = lerpedColor;
					}
				}
			} else {
				if (rend [0].material.color != Color.white) {
					for (int i = 0; i < rend.Length; i++) {
						if (rend[i] != null)
						{
							rend [i].material.color = Color.white;
						}
					}
				}
			}


			if (charController.isGrounded) 
			{
				jumptofall = 0;
				if (fallDmgToApply > 0) {
					Debug.Log ("applying " + fallDmgToApply + " dmg from falling");
					health -= fallDmgToApply;
					fallDmgToApply = 0;
				}
				lastGroundedHeight = transform.position.y;
				
				Vector3 fwd = transform.TransformDirection (-transform.right);
				
				RaycastHit hit;
				Ray ray = new Ray (raycast_obj.transform.position, fwd);
					Debug.DrawRay (raycast_obj.transform.position, fwd, Color.red, 2);

				if (Physics.Raycast (ray, out hit, stopDist_minion)) {
					if (hit.collider.gameObject.tag == "Minion") 
					{
						if (!hit.collider.GetComponent<MinionControls>().victory)
						{
							if (!anim.IsPlaying ("Agro")) 
							{
								anim.CrossFade ("Agro");
							}
							
							moveDirection = Vector3.zero;
							impatience_timer += Time.deltaTime;

							if (impatience_timer > impatience_Time)
							{
								Deal_damage (hit.collider.gameObject);
							}

							hitMinion = true;
						}
					} 
					else 
					{
						hitMinion = false;
					}
				} 
				else 
				{
					hitMinion = false;
				}
				
				
				if (!hitMinion) 
				{
					RaycastHit hit2;
					Quaternion spreadAngle = Quaternion.AngleAxis (15, Vector3.forward);
					Vector3 newVector = spreadAngle * fwd;
					
					Ray ray2 = new Ray (raycast_obj1.transform.position, newVector);
					if (Physics.Raycast (ray2, out hit2, stopDist, 1 << 8)) 
					{
						moveDirection = Vector3.zero;
						if (!anim.IsPlaying ("Idle")) {
							anim.CrossFade ("Idle");
						}
					} 
					else 
					{
						if (!anim.IsPlaying ("Walk")) 
						{
							anim.CrossFade ("Walk");
						}
						moveDirection = fwd;
						moveDirection *= Move_Speed;
					
					} 
				}
			}
			else 
			{
				//falldmg
				float fallDist = lastGroundedHeight - transform.position.y;
				
				if (fallDist > 0 || fallDist < -1) {
					if (jumptofall == 0) {
						anim.CrossFade ("Jump");
						if (Time.timeSinceLevelLoad > timeLastJumpSoundPlayed +1)
						{
							int jumpIndex = Random.Range(0,jumpsounds.Length);
							jumpsounds[jumpIndex].Play();
							timeLastJumpSoundPlayed = Time.timeSinceLevelLoad;
						}
						jumptofall++;
					} else {
						anim.CrossFadeQueued ("Fall");
					}
				}
				if (fallDist > fallDMGHeightStart) {
					fallDmgToApply = fallDist * ((fallDist - fallDMGHeightStart) * fallDMGMultip);
				} else {
					fallDmgToApply = 0;
				}
				
				moveDirection.y -= gravity * Time.deltaTime;
			}
			charController.Move (moveDirection * Time.deltaTime);
		}
	}
	
	void Deal_damage(GameObject hit) 
	{
		anim.Play ("Push");
		hit.GetComponent<MinionControls>().health -= 100;
		impatience_timer = 0;

		if (attackSounds.Length > 0)
		{
			int attackSoundIndex = Random.Range(0, attackSounds.Length);
			attackSounds[attackSoundIndex].Play();
		}
	}
	
	void OnTriggerEnter(Collider col) 
	{
		Rigidbody body = col.attachedRigidbody;
		if (body == null || body.isKinematic)
			return;

		if (charController.isGrounded) {
			body.AddForce (-transform.up * 100);
			if (fallDmgToApply > 0)
			{
				body.AddForce (-transform.up * 100 * fallDmgToApply);
			}
			
			Explodable expl = col.GetComponent<Explodable>();
			if (expl)
			{
				expl.isActive = true;
			}
		}
		
		if (body.velocity.magnitude > dropDMGStart && body.mass >= 1)
		{
			health -= 100;
		}
	}

	IEnumerator DestroyTime(float time)
	{
		yield return new WaitForSeconds(time);
		if (destroy_Effect != null)
		{
			for (int i = 0; i < rend.Length; i++) {
				if (rend[i] != null)
				{
					Instantiate(destroy_Effect, rend[i].transform.position, rend[i].transform.rotation);
				}
			}
		}
		Destroy (gameObject);
	}
}
