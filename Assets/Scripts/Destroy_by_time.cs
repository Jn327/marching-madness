﻿using UnityEngine;
using System.Collections;

public class Destroy_by_time : MonoBehaviour 
{
	public float Lifetime;

	// Use this for initialization
	void Start () 
	{
		Destroy (gameObject, Lifetime);
	}
}
