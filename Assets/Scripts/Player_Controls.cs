﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player_Controls : MonoBehaviour 
{
	private Ray click_ray;
	private GameGUI gameGui;
	private RaycastHit click_ray_hit;

	public clickInfo[] clicksInfo;
	public GameObject handleTransformPrefab;
	Touch[] myTouches;


	// Use this for initialization
	void Start () 
	{    
		gameGui = GetComponent<GameGUI> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Time.timeScale > 0.5f) 
		{
			selecting ();
		}
	}
	void LateUpdate () 
	{
		move_object ();
	}
	
	
	void selecting ()
	{
		//use multitouch input and addforceatposition to a child gameobject...
		myTouches = Input.touches;
		//if there is a touch
		if (myTouches.Length > 0)
		{
			for (int i = 0; i < Input.touchCount; i++) 
			{
				if (i <= clicksInfo.Length)
				{
					switch (myTouches[i].phase) 
					{
					case TouchPhase.Began:
						TouchBeginLogic(i);
						break;
						
					case TouchPhase.Moved:
						TouchDownLogic(i);
						break;
						
					case TouchPhase.Ended:
						TouchUpLogic(i);
						break;
					}
				}
			}
		}
		else
		{
			//no touch use the first object
			if (Input.GetMouseButtonDown (0)) 
			{
				TouchBeginLogic(0);
			}

			if (Input.GetMouseButton (0)) 
			{
				TouchDownLogic(0);
			}

			if (Input.GetMouseButtonUp (0)) 
			{			
				TouchUpLogic(0);
			}
		}

	}

	void move_object ()
	{
		if (clicksInfo.Length > 0) 
		{
			for (int i = 0; i < clicksInfo.Length; i++) 
			{
				if (clicksInfo[i].clicked_object != null)
				{
					Rigidbody rb = clicksInfo[i].clicked_object.GetComponent<Rigidbody>();
					if (rb != null)
					{
						Vector3 mousepos = Camera.main.ScreenToWorldPoint (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, -1));
						mousepos = new Vector3 (mousepos.x, mousepos.y, -1);
						Vector3 handlePos = new Vector3(clicksInfo[i].clicked_object_Handle.transform.position.x, clicksInfo[i].clicked_object_Handle.transform.position.y, -1);
						Vector3 mouseDir = mousepos - handlePos;
						rb.AddForceAtPosition(mouseDir * 100, handlePos);
						if (Application.isMobilePlatform)
						{
							rb.AddForceAtPosition(mouseDir * 250, handlePos);
						}
					}


					if (clicksInfo[i].clicked_object.transform.position.y > 20)
					{
						rb.drag = 0;
						//rb.useGravity = true;
						clicksInfo[i].clickStartPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

						//gameGui.LookAmount = transform.position.x;

						clicksInfo[i].clicked_object = null;
					}
				}
			}
		}
	}


	public void TouchBeginLogic (int i)
	{
		click_ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		
		clicksInfo[i].clickStartPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		
		if (Physics.Raycast (click_ray, out click_ray_hit, Mathf.Infinity, 1 << 9))
		{
			GameObject clickGO = click_ray_hit.transform.gameObject;
			
			Rigidbody rb = clickGO.GetComponent<Rigidbody>();
			if (rb.mass < 2.5f)
			{
				clicksInfo[i].clicked_object = clickGO;
				
				rb.drag = 10;
				//rb.useGravity = false;

				//create the handle gameobject
				clicksInfo[i].clicked_object_Handle = Instantiate(handleTransformPrefab, clicksInfo[i].clickStartPos, Quaternion.identity) as GameObject;
				clicksInfo[i].clicked_object_Handle.transform.parent = clicksInfo[i].clicked_object.transform;
				clicksInfo[i].clicked_object_Handle.transform.position = new Vector3(clicksInfo[i].clicked_object_Handle.transform.position.x, clicksInfo[i].clicked_object_Handle.transform.position.y, 0);

				clicksInfo[i].clicked_object.transform.position = new Vector3(clicksInfo[i].clicked_object.transform.position.x,clicksInfo[i].clicked_object.transform.position.y, -1);
			}
		}
	}

	public void TouchDownLogic (int i)
	{
		if (clicksInfo[i].clicked_object == null)
		{
			clicksInfo[i].clickEndPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			
			Vector3 dragDir = clicksInfo[i].clickEndPos - clicksInfo[i].clickStartPos;
			gameGui.LookAmount -= dragDir.x/10;
		}
	}

	public void TouchUpLogic (int i)
	{
		clicksInfo[i].clickEndPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		Vector3 dragDir = clicksInfo[i].clickEndPos - clicksInfo[i].clickStartPos;
		
		if (clicksInfo[i].clicked_object != null)
		{
			Rigidbody rb = clicksInfo[i].clicked_object.GetComponent<Rigidbody>();
			//rb.isKinematic = false;
			rb.drag = 0;
			//rb.useGravity = true;
			gameGui.LookAmount = transform.position.x;
			
			clicksInfo[i].clicked_object = null;
			Destroy(clicksInfo[i].clicked_object_Handle);
			clicksInfo[i].clicked_object_Handle = null;
		}
	}
}

[System.Serializable]
public class clickInfo
{
	public GameObject clicked_object;
	public GameObject clicked_object_Handle;
	
	public Vector3 clickStartPos;
	public Vector3 clickEndPos;
}
