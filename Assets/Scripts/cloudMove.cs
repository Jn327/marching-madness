﻿using UnityEngine;
using System.Collections;

public class cloudMove : MonoBehaviour {

	float moveAmount = 0.5f;
	float speed = 0.2f;
	float timer = 0;
	public Vector3 initialPos;
	// Use this for initialization
	void Start () {
		initialPos = transform.position;
		//speed = Random.Range (0.1f, 0.25f);
		//moveAmount = Random.Range (0.1f, 2);
	}
	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;
		transform.position = initialPos + (Mathf.PingPong (timer * speed, moveAmount)*Vector3.right);
	}
}
