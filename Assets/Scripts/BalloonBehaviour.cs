﻿using UnityEngine;
using System.Collections;

public class BalloonBehaviour : MonoBehaviour {

	public GameObject projectile;
	public GameObject attackPos;
	float attackTimer = 0.33f;
	public float attackFreq = 2;

	public float forceAmount = 500;
	Vector3 initialPos;
	Quaternion initialRot;

	public GameObject[] destroyParts;
	public Transform[] destroyPositions;


	
	// Use this for initialization
	void Start () {
		initialPos = transform.position;
		initialRot = transform.rotation;
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		if (projectile != null) {
			attackTimer += Time.deltaTime;
			if (attackTimer >= attackFreq) {
				Instantiate (projectile, attackPos.transform.position, attackPos.transform.rotation);
				//projectile.GetComponent<Rigidbody> ().AddForce (attackPos.transform.right * forceAmount * Random.Range (0.75f, 1.5f));
				attackTimer = 0;
			}
		}

		transform.position = initialPos + (Mathf.PingPong (Time.time, 20)*Vector3.right) + (Mathf.PingPong (Time.time/2, 1)*Vector3.up);
		transform.rotation = initialRot * Quaternion.AngleAxis (Mathf.PingPong (Time.time*2, 15), Vector3.up);
	}

	public void Die()
	{
		for (int i = 0; i <destroyParts.Length; i++)
		{
			Instantiate(destroyParts[i],destroyPositions[i].position, destroyPositions[i].rotation);
		}
		Destroy (gameObject);
	}



	void OnCollisionEnter(Collision collision) {
		if (collision.relativeVelocity.magnitude > 10) {
			Die();
		}
	}

}
