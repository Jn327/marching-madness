﻿using UnityEngine;
using System.Collections;

public class destroyTimer : MonoBehaviour {

	float LifeTime = 2;
	public Transform[] destroy_Loc;
	public GameObject destroy_Effect;

	// Use this for initialization
	void Start () {
		StartCoroutine("DestroyTime",LifeTime);
	}

	IEnumerator DestroyTime(float time)
	{
		yield return new WaitForSeconds(time);
		if (destroy_Effect != null)
		{
			for (int i = 0; i < destroy_Loc.Length; i++) {
				if (destroy_Loc[i] != null)
				{
					Instantiate(destroy_Effect, destroy_Loc[i].position, destroy_Loc[i].rotation);
				}
			}
		}
		Destroy (gameObject);
	}
}
