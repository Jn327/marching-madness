﻿using UnityEngine;
using System.Collections;

public class ParticleSystemMultipleMats : MonoBehaviour {

	public Material[] mats;
	public bool isRandomofMats = false;

	// Use this for initialization
	void Start () 
	{
		if (isRandomofMats) {
			int matIndex = Random.Range (0, mats.Length);
			GetComponent<ParticleSystem> ().GetComponent<Renderer> ().material = mats[matIndex];
		} else {
			GetComponent<ParticleSystem> ().GetComponent<Renderer> ().materials = mats;
		}
	}
}
