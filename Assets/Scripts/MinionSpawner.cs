﻿using UnityEngine;
using System.Collections;

public class MinionSpawner : MonoBehaviour {

	public GameObject minionPrefab;
	public GameObject minionPrefab_Leader;
	float minionSpawnTimer = 0.0f;
	public float minionSpawnFreq = 2;
	public int numberOfminionsSpawned = 0;

	GameGUI gameGUI;
	void Start ()
	{
		minionSpawnTimer = minionSpawnFreq;
		gameGUI = Camera.main.GetComponent<GameGUI> ();
	}

	// Update is called once per frame
	void Update () 
	{
		minionSpawnTimer += Time.deltaTime;
		if (minionSpawnTimer >= minionSpawnFreq) 
		{
			if (gameGUI != null)
			{
				if (numberOfminionsSpawned < gameGUI.number_Minions)
				{
					if (numberOfminionsSpawned == 0)
					{
						minionSpawnTimer = 0;
						//spawn a minion
						GameObject minionGo = Instantiate(minionPrefab_Leader, transform.position, Quaternion.identity) as GameObject;
						gameGUI.minions.Add(minionGo);
						numberOfminionsSpawned ++;
					}

					else
					{
						minionSpawnTimer = 0;
						//spawn a minion
						GameObject minionGo = Instantiate(minionPrefab, transform.position, Quaternion.identity) as GameObject;
						gameGUI.minions.Add(minionGo);
						numberOfminionsSpawned ++;
					}
				}
			}
			else
			{
				minionSpawnTimer = 0;
				Instantiate(minionPrefab, transform.position, Quaternion.identity);
			}
		}


	}
}
