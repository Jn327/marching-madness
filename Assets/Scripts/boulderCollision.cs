﻿using UnityEngine;
using System.Collections;

public class boulderCollision : MonoBehaviour {
	public GameObject explosion;
	public GameObject crashSound;

	void OnCollisionEnter (Collision collision)
	{
		if (collision.relativeVelocity.magnitude > 10) {
			Instantiate (explosion, transform.position, transform.rotation);
			if (crashSound != null) {
				Instantiate (crashSound, transform.position, transform.rotation);
			}
			Destroy (gameObject);
		}
	}
}
