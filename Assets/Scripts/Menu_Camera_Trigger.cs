﻿using UnityEngine;
using System.Collections;

public class Menu_Camera_Trigger : MonoBehaviour 
{
	void OnTriggerEnter (Collider other)
	{
		if (other != null) 
		{
			Debug.Log ("Trigger");
			Camera.main.transform.GetComponent<MainMenu> ().shatter = true;
		}
	}
}
