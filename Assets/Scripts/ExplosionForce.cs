﻿using UnityEngine;
using System.Collections;

public class ExplosionForce : MonoBehaviour {

	public float radius = 5.0F;
	public float power = 10.0F;

	//public GameObject destroy_Effect;
	
	void Start() {
		Vector3 explosionPos = transform.position;
		Collider[] colliders = Physics.OverlapSphere(explosionPos, radius);
		foreach (Collider hit in colliders) {
			Rigidbody rb = hit.GetComponent<Rigidbody>();
			
			if (rb != null)
				rb.AddExplosionForce(power, explosionPos, radius, 3.0F);


			if (hit.tag == "Minion")
			{
				MinionControls mincont = hit.gameObject.GetComponent<MinionControls>();
				mincont.health -= 100;
				Vector3 forceDir = hit.gameObject.transform.position - transform.position;
				for (int i = 0; i < mincont.body_parts.Length; i++)
				{
					if (mincont.body_parts[i] != null)
					{
						mincont.body_parts[i].AddForce(forceDir * 500);
					}
				}
			}
			if (hit.tag == "Catapult")
			{
				hit.gameObject.GetComponent<TrebuchetBehaviour>().GetDestroyed();
			}

			if (hit.tag == "Ballon")
			{
				hit.gameObject.GetComponent<BalloonBehaviour>().Die();
			}


			//test for a tnt
			Explodable exp = hit.gameObject.GetComponent<Explodable>();
			if (exp)
			{
				exp.Explode();
			}
		}
		float t = transform.GetComponent<ParticleSystem> ().duration;
		StartCoroutine("DestroyTime",t);
	}

	IEnumerator DestroyTime(float time)
	{
		yield return new WaitForSeconds(time);
		/*if (destroy_Effect != null)
		{
			Instantiate(destroy_Effect, transform.position, transform.rotation);
		}*/
		Destroy (gameObject);
	}
}
