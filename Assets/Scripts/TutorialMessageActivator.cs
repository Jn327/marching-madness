﻿using UnityEngine;
using System.Collections;

public class TutorialMessageActivator : MonoBehaviour {

	public int menuPos;

	void OnTriggerEnter (Collider other)
	{
		if (other.tag == "Minion")
		{
			GameGUI gamegui = Camera.main.GetComponent<GameGUI> ();
			gamegui.SetMenuPos (menuPos);
			gamegui.PauseClick ();

			Destroy (gameObject);
		}
	}
}
