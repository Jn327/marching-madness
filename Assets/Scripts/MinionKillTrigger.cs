﻿using UnityEngine;
using System.Collections;

public class MinionKillTrigger : MonoBehaviour {

	[HideInInspector]
	public GameGUI gameGui;
	public GameObject splash_Particles;
	bool splaph_spawned;
	// Use this for initialization
	void Start ()
	{
		gameGui = Camera.main.transform.GetComponent<GameGUI> ();
	}

	
	void OnTriggerEnter (Collider other)
	{
		if (other.tag == "Minion") 
		{
			gameGui.minions.Remove (other.gameObject);
		}
			//Destroy(other.gameObject);
			Instantiate (splash_Particles, new Vector3 (other.transform.position.x, other.transform.position.y, 0), Quaternion.identity);
			Destroy (other.gameObject);

	}
}

