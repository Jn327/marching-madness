﻿using UnityEngine;
using System.Collections;

public class MinionControls : MonoBehaviour {
	
	CharacterController charController;
	public Renderer[] rend;
	public Animation anim;
	public float Move_Speed = 5;
	public float gravity = 50;
	private Vector3 moveDirection = Vector3.zero;
	public float stopDist = 2;
	public float stopDist_minion;
	public float health = 100;

	public GameObject icecream;
	public GameObject spear;

	public GameObject destroy_Effect;
	
	public float impatience_timer;
	public float impatience_Time = 3;
	bool impatient = false; 
	public float pushPower = 5;

	[HideInInspector]
	public GameGUI gameGui;
	
	float fallDmgToApply = 0;
	float lastGroundedHeight = 0;
	public float fallDMGHeightStart = 5;
	public float fallDMGMultip = 2;
	
	public float dropDMGStart =5f;

	public GameObject raycast_obj;
	public GameObject raycast_obj1;
	
	bool hitMinion = false;

	int jumptofall = 0;

	public Rigidbody [] body_parts;

	public AudioSource[] DieSound;
	bool dieSoundPlayed = false;

	public AudioSource[] SpawnSound;

	bool hasPlayedTrumpet = false;
	public bool victory = false;

	public GameObject[] footSteps;

	public AudioSource[] jumpsounds;
	float timeLastJumpSoundPlayed = 0;

	public AudioSource[] VictorySound;
	bool VictorySoundPlayed = false;
	
	
	// Use this for initialization
	void Start () 
	{
		charController = GetComponent<CharacterController> ();
		gameGui = Camera.main.transform.GetComponent<GameGUI> ();

		if (SpawnSound.Length > 0) {
			int randSpawnSound = Random.Range (0, SpawnSound.Length);
			SpawnSound [randSpawnSound].Play ();
		}

		if (footSteps.Length > 0) {
			int randIndex = Random.Range (0, footSteps.Length);
			footSteps[randIndex].SetActive(true);
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (victory) {
			if (!VictorySoundPlayed && VictorySound.Length > 0)
			{
				int randIndex = Random.Range (0, VictorySound.Length);
				VictorySound[randIndex].Play();
				VictorySoundPlayed = true;
			}
			if (!hasPlayedTrumpet)
			{
				anim.CrossFade ("Trumpet");
				hasPlayedTrumpet = true;
				if (icecream != null)
				{
					icecream.SetActive(true);
				}
				if (spear != null)
				{
					spear.SetActive(false);
				}
			}
			else
			{
				if (!anim.IsPlaying("Trumpet"))
				{
					if (!anim.IsPlaying("Agro"))
					{
						anim.CrossFade ("Agro");
					}
					if (charController.isGrounded)
					{
						moveDirection = transform.TransformDirection(transform.right);
					}
					else
					{
						moveDirection = Vector3.zero;
					}
					moveDirection.y -= gravity * Time.deltaTime;
					charController.Move (moveDirection * Time.deltaTime *Move_Speed);
				}
			}
		} else {
			if (health <= 0) {
				if (!dieSoundPlayed) 
				{
					int soundIndex = Random.Range(0,DieSound.Length);
					DieSound[soundIndex].Play ();
					dieSoundPlayed = true;
				}
				for (int i = 0; i < body_parts.Length; i++) {
					if (body_parts[i] != null)
					{
						anim.Stop ();
						body_parts [i].isKinematic = false;
						body_parts [i].useGravity = true;
					}
				}
				charController.enabled = false;
				if (gameGui != null)
				{
					gameGui.minions.Remove (gameObject);
				}
				StartCoroutine("DestroyTime",5);
			} else {
				if (impatience_timer >= impatience_Time - 1.5f && !impatient) {
					float ping = Mathf.PingPong (impatience_timer*3, 1);
					/*float speed = 1 - (impatience_timer / impatience_Time);
				ping = (Mathf.Sin (Time.time * speed) + 1.0f) / 2.0f;*/
					Color lerpedColor = Color.Lerp (Color.white, Color.red, ping);
					for (int i = 0; i < rend.Length; i++) {
						if (rend[i] != null)
						{
							rend [i].material.color = lerpedColor;
						}
					}
				} else {
					if (rend [0].material.color != Color.white) {
						for (int i = 0; i < rend.Length; i++) {
							if (rend[i] != null)
							{
								rend [i].material.color = Color.white;
							}
						}
					}
				}
				if (charController.isGrounded) {
					jumptofall = 0;
					if (fallDmgToApply > 0) {
						Debug.Log ("applying " + fallDmgToApply + " dmg from falling");
						health -= fallDmgToApply;
						fallDmgToApply = 0;
					}
					lastGroundedHeight = transform.position.y;
			
					Vector3 fwd = transform.TransformDirection (transform.right);
			
					RaycastHit hit;
					Ray ray = new Ray (raycast_obj.transform.position, Vector3.right);
					if (Physics.Raycast (ray, out hit, stopDist_minion)) {
						if (hit.collider.gameObject.tag == "Minion" || hit.collider.gameObject.tag == "EvilMinion") {
							if (hit.collider.gameObject.tag == "Minion")
							{
								anim.CrossFade ("Idle");
								hitMinion = true;
							}
							else
							{
								anim.CrossFade ("Idle");
								hitMinion = true;
							}
						} else {
							hitMinion = false;
						}
					} else {
						hitMinion = false;
					}
			
			
					if (!hitMinion) {
						RaycastHit hit2;
						Vector3 noAngle = transform.right;
						Quaternion spreadAngle = Quaternion.AngleAxis (15, Vector3.forward);
						Vector3 newVector = spreadAngle * noAngle;

						Ray ray2 = new Ray (raycast_obj1.transform.position, newVector);
						if (Physics.Raycast (ray2, out hit2, stopDist, 1 << 8) && impatience_timer < impatience_Time) {
							moveDirection = Vector3.zero;
							impatience_timer += Time.deltaTime;
							if (impatience_timer > impatience_Time) {
								impatient = true;
							}

							if (!impatient) {
								if (!anim.IsPlaying ("Idle")) {
									anim.CrossFade ("Idle");
								}
							}
						} else {
							if (impatience_timer > 0) {
								impatience_timer -= Time.deltaTime;
							} else {
								impatient = false;
							}

							if (!impatient) {
								if (!anim.IsPlaying ("Walk")) {
									anim.CrossFade ("Walk");
								}
							} else {
								if (!anim.IsPlaying ("Push")) {
									anim.CrossFade ("Push");
								}
							}
							moveDirection = fwd;
							moveDirection *= Move_Speed;
						}
					} else {
						moveDirection = Vector3.zero;
					}
				} else {
					//falldmg
					float fallDist = lastGroundedHeight - transform.position.y;

					if (fallDist > 0 || fallDist < -1) {
						if (jumptofall == 0) {
							anim.CrossFade ("Jump");
							if (Time.timeSinceLevelLoad > timeLastJumpSoundPlayed +1)
							{
								int jumpIndex = Random.Range(0,jumpsounds.Length);
								jumpsounds[jumpIndex].Play();
								timeLastJumpSoundPlayed = Time.timeSinceLevelLoad;
							}
							jumptofall++;
						} else {
							anim.CrossFadeQueued ("Fall");
						}
					}
					if (fallDist > fallDMGHeightStart) {
						fallDmgToApply = fallDist * ((fallDist - fallDMGHeightStart) * fallDMGMultip);
					} else {
						fallDmgToApply = 0;
					}
			
				}
				moveDirection.y -= gravity * Time.deltaTime;
				charController.Move (moveDirection * Time.deltaTime);
			}
		}
	}
	
	void OnControllerColliderHit(ControllerColliderHit hit) 
	{
		Rigidbody body = hit.collider.attachedRigidbody;
		if (body == null || body.isKinematic)
			return;

		Vector3 dir = transform.position - hit.point;
		body.AddForceAtPosition (dir * 50, hit.point);

		if (charController.isGrounded) {

			Explodable expl = hit.collider.GetComponent<Explodable>();
			if (expl)
			{
				expl.isActive = true;
			}
		}

		if (impatient && body.mass <= 2) 
		{
			RaycastHit hit3;
			Ray ray = new Ray (raycast_obj.transform.position, Vector3.right);
			if (Physics.Raycast (ray, out hit3, stopDist, 1 << 8))
			{
				//if im impatient then do some pushing
				Vector3 pushDir = transform.right;
				body.velocity = pushDir * pushPower;
			}
			else
			{
				impatient = false;
			}
		}
		else
		{
			impatience_timer = 0;
			impatient = false;
		}
	}
	
	void OnTriggerEnter(Collider col) 
	{
		Rigidbody body = col.attachedRigidbody;
		if (body == null || body.isKinematic)
			return;

		if (body.velocity.magnitude > dropDMGStart && body.mass >= 1)
		{
			//health -= collision.relativeVelocity.y * ((collision.relativeVelocity.y-dropDMGStart) * dropDMGMultip);
			health -= 100;
		}
	}

	IEnumerator DestroyTime(float time)
	{
		yield return new WaitForSeconds(time);
		if (destroy_Effect != null)
		{
			for (int i = 0; i < rend.Length; i++) {
				if (rend[i] != null)
				{
					Instantiate(destroy_Effect, rend[i].transform.position, rend[i].transform.rotation);
				}
			}
		}
		Destroy (gameObject);
	}
}
