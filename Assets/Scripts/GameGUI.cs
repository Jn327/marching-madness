﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;


public class GameGUI : MonoBehaviour {
	
	bool paused = false;
	Player_Controls controls;
	
	public List<GameObject> minions = new List<GameObject>();
	public int minionsSaved = 0;

	public int number_Minions; //how many minions to spawn at the start of the game...

	Vector3 centerTransform;
	public float moveSpeed = 1;

	public float LookAmount = 0;
	

	MinionSpawner minionSpawner;

	 Vector3 spawnPoint;
	 Vector3 Endpoint;

	public AudioSource gameOverSound;
	bool gameoverSoundPlayed = false;

	public Sprite[] badges;
	int badgeIndex = 0;

	public int currentLevelBadgeIndex = -1;


	public menuWindow[] menuWindows;
	public int menuIndex = 0;
	bool menuSet = false;
	public Slider speedSlider;
	bool gameOverShowed = false;
	public Text gameOverHeaderText;
	public Image badgeImage;
	public Text minionsSavedText;
	public GameObject nextLevelButton;
	float timeSinceLastDrag = 0;

	public List<RectTransform> levelButtons = new List<RectTransform> ();
	
	public GameObject LevelButton;
	public int[] LevelBadgeScores;
	int number_levels = 8;
	public float buttonSize = 75;

	public Text currentLVLTxt;
	
	void Start ()
	{
		number_levels = Application.levelCount;
		minionSpawner = GameObject.FindGameObjectWithTag ("MinionSpawner").GetComponent<MinionSpawner>();

		InvokeRepeating ("SetCenterTransform", 1, 1);
		Time.timeScale = 1;

		controls = Camera.main.transform.GetComponent<Player_Controls> ();
		
		
		number_Minions = PlayerPrefs.GetInt ("number_Minions", 5);

		spawnPoint = GameObject.FindGameObjectWithTag ("MinionSpawner").transform.position;
		Endpoint = GameObject.FindGameObjectWithTag ("IceCreamShop").transform.position;

		currentLevelBadgeIndex = PlayerPrefs.GetInt ("LevelScore"+Application.loadedLevel, -1);
		currentLVLTxt.text = "Current Level: " +Application.loadedLevel;

		LevelBadgeScores = new int[number_levels];
		for (int i = 1; i < number_levels; i++) 
		{
			LevelBadgeScores[i-1] = PlayerPrefs.GetInt ("LevelScore"+i, -1);
		}
		
		for (int i = 0; i < menuWindows.Length; i++)
		{
			//if there is an object to place the levels buttons in then go ahead and instantiate them...
			if (menuWindows[i].levelsParent != null)
			{
				menuWindows[i].levelsParent.sizeDelta = new Vector2((buttonSize * (number_levels)), menuWindows[i].levelsParent.sizeDelta.y);
				
				for (int b = 1; b <number_levels; b++)
				{
					GameObject button1 = Instantiate(LevelButton, Vector3.zero, Quaternion.identity) as GameObject;
					
					GameObject button = button1.transform.GetChild(0).gameObject;
					RectTransform RTransf = button1.GetComponent<RectTransform>();
					RTransf.SetParent(menuWindows[i].levelsParent.transform);
					RTransf.anchoredPosition = new Vector2(-(buttonSize * (number_levels)/2)+(buttonSize*(b)), 0);
					
					button.name += " "+b;
					
					LevelButtonScript lvlButton = button.GetComponent<LevelButtonScript>();
					lvlButton.levelIndex = b;
					lvlButton.SetScore(LevelBadgeScores[b-1]);
					
					button.GetComponentInChildren<Text>().text = "Level "+(b);
					menuWindows[i].elements.Add(button);
					levelButtons.Add(RTransf);
				}
			}
		}



		//make all the buttons inactive...
		for (int i = 0; i < menuWindows.Length; i++) 
		{
			for (int j = 0; j < menuWindows[i].elements.Count; j ++)
			{
				Button btn = menuWindows[i].elements[j].GetComponent<Button>();
				if (btn != null)
				{
					btn.enabled = false;
				}
			}
		}
	}
	
	void Update ()
	{

		if (!menuSet && Time.timeSinceLevelLoad > 1f)
		{
			SetMenuPos(0);
			menuSet = true;
		}

		if (LookAmount < spawnPoint.x)
		{
			LookAmount = spawnPoint.x;
		}
		if (LookAmount > Endpoint.x)
		{
			LookAmount = Endpoint.x;
		}
		transform.position = Vector3.Lerp (transform.position, new Vector3(LookAmount, transform.position.y, transform.position.z), moveSpeed * Time.deltaTime);
		if (!Input.GetMouseButton (0)) 
		{
			//float lookDifference = Vector3.Distance(new Vector3(LookAmount,0,0), centerTransform);
			timeSinceLastDrag += Time.deltaTime * 2;
			LookAmount = Mathf.MoveTowards (LookAmount, centerTransform.x, Time.deltaTime * moveSpeed * timeSinceLastDrag);
		}
		else
		{
			timeSinceLastDrag = 0;
		}
		
		if (minionSpawner)
		{
			if (minions.Count <= 0 && minionSpawner.numberOfminionsSpawned >= number_Minions) //Game Over!!!!
			{
				if (!gameOverShowed)
				{
					paused = false;
					SetMenuPos(2);
					gameOverShowed = true;

					//set the header to different messaged depending on the players score...
					gameOverHeaderText.text = "Game Over!";

					minionsSavedText.text = "" +minionsSaved +"/" +number_Minions+" minions got ice-cream.";
					//set the badge...
					if (minionsSaved > 0)
					{
						gameOverHeaderText.text = "Good Job!";

						float normalizedScore = (float)minionsSaved/(float)number_Minions;
						normalizedScore *= (float)(badges.Length-1);
						badgeIndex = Mathf.RoundToInt(normalizedScore);

						if (badgeIndex > currentLevelBadgeIndex)
						{
							currentLevelBadgeIndex = badgeIndex;
							PlayerPrefs.SetInt ("LevelScore"+Application.loadedLevel, currentLevelBadgeIndex);
							gameOverHeaderText.text = "New high score";
						}

						badgeImage.sprite = badges[badgeIndex];
						badgeImage.color = Color.white;
					}


					//instantiate the 'next level' button if possible...
					if (minionSpawner.numberOfminionsSpawned > 0 && Application.loadedLevel < 5)
					{
						nextLevelButton.SetActive(true);
					}
				}

				if (gameOverSound != null && !gameoverSoundPlayed && minionsSaved <= 0)
				{
					gameOverSound.Play();
					gameoverSoundPlayed = true;
				}
			}
		}

		if (menuWindows [menuIndex].levelsScrollBar != null) {
			for (int i = 0; i < levelButtons.Count; i++) {
				if (levelButtons [i] != null) {
					//get the button position normalized from 0 to 1;
					float normalizedButtonPos = (float)i / (levelButtons.Count - 1);
					
					//get the normalized value of normalizedButtonPos as it approximates menuWindows [menuIndex].levelsScrollBar.value
					//if menuWindows [menuIndex].levelsScrollBar.value is one how close is normalizedButtonPos to it on a scale of 0 to 1.
					//anything more than 0.5f away from levelsScrollBar.val shall be 0.
					float distBetweencurrentAndVal = Mathf.Abs (normalizedButtonPos - menuWindows [menuIndex].levelsScrollBar.value);
					
					levelButtons [i].eulerAngles =new Vector3 (0, normalizedButtonPos - menuWindows [menuIndex].levelsScrollBar.value, 0) * 50;
					
					float normalizedSizeIncrease = (1 - (distBetweencurrentAndVal*2f));
					if (normalizedSizeIncrease < 0) {
						normalizedSizeIncrease = 0;
					}
					
					levelButtons [i].localScale = new Vector3 (normalizedSizeIncrease, normalizedSizeIncrease, 1);
					levelButtons[i].anchoredPosition = new Vector2(levelButtons[i].anchoredPosition.x, -distBetweencurrentAndVal*50);
					
					//for the organization depending on index.
					levelButtons [i].name = (1-distBetweencurrentAndVal).ToString ();
					
					Color newcol = new Color (1,1,1, 1-distBetweencurrentAndVal);
					levelButtons[i].GetComponent<Image>().color = newcol;
					GameObject button = levelButtons[i].transform.GetChild(0).gameObject;
					button.GetComponent<Image>().color = newcol;
					LevelButtonScript btnScript = button.GetComponent<LevelButtonScript>();
					if (btnScript.score >= 0)
					{
						btnScript.levelScoreIcon.color = newcol;
					}
					btnScript.LevelScreenShot.color = newcol;
				}
			}
			
			
			//sort the level buttons by their name...
			Transform[] levelButtonsArray = levelButtons.OrderBy  (go => go.name).ToArray();
			for (int i = 0; i <levelButtonsArray.Length; i++) {
				levelButtonsArray [i].SetSiblingIndex (i);
			}
		}
	}
	
	public void LoadMenu ()
	{
		Application.LoadLevel ("Menu");
	}

	public void backLevelSelect ()
	{
		if (!gameOverShowed)
		{
			SetMenuPos(1);
		}
		else
		{
			SetMenuPos(2);
		}
	}
	
	public void FollowMinions ()
	{
		SetCenterTransform ();
		LookAmount = centerTransform.x;
	}

	public void SetCenterTransform ()
	{
		if (minions.Count > 0) 
		{
			float furthestX = -100;
			int furthestIndex = 0;
			for (int i = 0; i < minions.Count; i++) {
				if (minions [i].transform.position.x > furthestX) {
					furthestX = minions [i].transform.position.x;
					furthestIndex = i;
				}
			}
			
			centerTransform = new Vector3 (minions [furthestIndex].transform.position.x, minions [furthestIndex].transform.position.y, minions [furthestIndex].transform.position.z);
		}
	}

	public void PauseClick()
	{
		if (paused)
		{
			setTimeScale();
			paused = false;
		}
		else
		{
			Time.timeScale = 0.0f;
			paused = true;
		}
	}


	public void SetMenuPos(int index)
	{
		StartCoroutine ("animateAndChangeMenuPos", index);
	}
	
	public IEnumerator animateAndChangeMenuPos(int index)
	{
		//make the old buttons dissapear
		for (int i = 0; i < menuWindows[menuIndex].elements.Count; i++) 
		{
			Button btn = menuWindows[menuIndex].elements[i].GetComponent<Button>();
			if (btn != null)
			{
				btn.enabled = false;
			}
			menuWindows[menuIndex].elements[i].GetComponent<Animator>().SetTrigger("Dissappear");
			//little work around as yield waitforseconds does not work while time.timescale <= 0
			float pauseEndTime = Time.realtimeSinceStartup + 0.05f;
			while (Time.realtimeSinceStartup < pauseEndTime)
			{
				yield return 0;
			}
		}
		
		//change the menu index
		menuIndex = index;
		
		
		//animate the new buttons to appear
		for (int i = 0; i < menuWindows[menuIndex].elements.Count; i++) 
		{
			Button btn = menuWindows[menuIndex].elements[i].GetComponent<Button>();
			if (btn != null)
			{
				btn.enabled = true;
			}
			menuWindows[menuIndex].elements[i].GetComponent<Animator>().SetTrigger("Appear");
			//little work around as yield waitforseconds does not work while time.timescale <= 0
			float pauseEndTime = Time.realtimeSinceStartup + 0.075f;
			while (Time.realtimeSinceStartup < pauseEndTime)
			{
				yield return 0;
			}
		}

		if (menuWindows[menuIndex].levelsScrollBar != null)
		{
			menuWindows[menuIndex].levelsScrollBar.size = 0.1f;
			menuWindows[menuIndex].levelsScrollBar.value = ((float)Application.loadedLevel)/(float)number_levels;
		}

		yield return null;
	}


	public void RestartLvl ()
	{
		Application.LoadLevel(Application.loadedLevel);
	}

	public void NextLvl ()
	{
		Application.LoadLevel(Application.loadedLevel+1);
	}

	public void QuitGame ()
	{
		Application.Quit();
	}

	public void setTimeScale ()
	{
		Time.timeScale = speedSlider.value;
	}
}
