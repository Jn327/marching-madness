﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

//Older version of the UIController script, I wrote for brains eden 2015 Games Jam
//This one has some dynamically created buttons LevelButton, they aren't pooled or 
//anything fancy, but its probably the best example I have for now of creating buttons and such at runtime...


public class MainMenu : MonoBehaviour {

	public GUISkin guiskin;
	public menuWindow[] menuWindows;
	int menuIndex = 0;
	bool menuPosSet = false;

	public List<RectTransform> levelButtons = new List<RectTransform> ();

	public GameObject LevelButton;

	public float buttonSize = 75;

	int number_Minions; //how many minions to spawn at the start of the game...
	public Text numberMinionsText;
	public Slider No_Minions_Slider;

	public Texture2D Logo;

	public GameObject explosive;
	public Texture2D [] glass_shatter;
	private int glass_shatter_count;
	public bool shatter;
	bool shatterSoundPlayed = false;
	private int index;
	private float shatter_timer;
	public float shatter_Time;
	bool levelSelected = false;

	public Texture2D splash;
	public Texture2D splashBG;

	public int[] LevelBadgeScores;
	int number_levels = 8;
	public Texture2D[] badgeTextures;
	float badgeSize = 70;

	Vector2 scrollPosition = Vector2.zero;
	int previousMenuPos = 0;

	void Start ()
	{
		number_levels = Application.levelCount;
		Time.timeScale = 1.0f;

		number_Minions = PlayerPrefs.GetInt ("number_Minions", 5);
		numberMinionsText.text = "Number Of Minions: " + number_Minions;
		No_Minions_Slider.value = number_Minions;

		LevelBadgeScores = new int[number_levels];
		for (int i = 1; i < number_levels; i++) 
		{
			LevelBadgeScores[i-1] = PlayerPrefs.GetInt ("LevelScore"+i, -1);
		}

		for (int i = 0; i < menuWindows.Length; i++)
		{
			//if there is an object to place the levels buttons in then go ahead and instantiate them...
			if (menuWindows[i].levelsParent != null)
			{
				menuWindows[i].levelsParent.sizeDelta = new Vector2((buttonSize * (number_levels)), menuWindows[i].levelsParent.sizeDelta.y);

				for (int b = 1; b <number_levels; b++)
				{
					GameObject button1 = Instantiate(LevelButton, Vector3.zero, Quaternion.identity) as GameObject;

					GameObject button = button1.transform.GetChild(0).gameObject;
					RectTransform RTransf = button1.GetComponent<RectTransform>();
					RTransf.SetParent(menuWindows[i].levelsParent.transform);
					RTransf.anchoredPosition = new Vector2(-(buttonSize * (number_levels)/2)+(buttonSize*(b)), 0);

					button.name += " "+b;

					LevelButtonScript lvlButton = button.GetComponent<LevelButtonScript>();
					lvlButton.levelIndex = b;
					lvlButton.SetScore(LevelBadgeScores[b-1]);

					Text txt = button.GetComponentInChildren<Text>(); 
					if (txt != null)
					{
						txt.text = "Level "+(b);
					}
					menuWindows[i].elements.Add(button);
					levelButtons.Add(RTransf);
				}
			}
		}

		//make all the buttons inactive...
		for (int i = 0; i < menuWindows.Length; i++) 
		{
			for (int j = 0; j < menuWindows[i].elements.Count; j ++)
			{
				Button btn = menuWindows[i].elements[j].GetComponent<Button>();
				if (btn != null)
				{
					btn.enabled = false;
				}
			}
		}
	}

	void OnGUI ()
	{
		float nativewidth = 1920;
		float nativeheight = 1080;
		
		float rx = Screen.width / nativewidth;
		float ry = Screen.height / nativeheight;
		
		GUI.matrix = Matrix4x4.TRS (new Vector3 (0, 0, 0), Quaternion.identity, new Vector3 (rx, ry, 1));
		
		GUI.skin = guiskin;

		if  (Time.time <= 2 && splash != null && splashBG != null) 
		{
			GUI.DrawTexture(new Rect(0,0,nativewidth,nativeheight), splashBG);
			GUI.DrawTexture(new Rect(100,0,nativewidth-200,nativeheight), splash);
		} 
		else 
		{
			if (shatter) 
			{
				GUI.DrawTexture (new Rect (0, 0, nativewidth, nativeheight), glass_shatter [glass_shatter_count]);
			} 
			if (!levelSelected) 
			{
				GUI.DrawTexture (new Rect (nativewidth / 2 - 512, 0, 1024, 256), Logo);

			}
		}
		if (!menuPosSet && Time.time > (2-(menuWindows[menuIndex].elements.Count*0.2f)))
		{
			SetMenuPos(0);
			menuPosSet = true;
		}
	}

	void FixedUpdate ()
	{
		if (shatter) 
		{
			if (!shatterSoundPlayed)
			{
				GetComponent<AudioSource>().Play();
				shatterSoundPlayed = true;
			}
			shatter_timer += Time.deltaTime;

			if (shatter_timer > shatter_Time)
			{
				if (glass_shatter_count < glass_shatter.Length - 1)
				{
					glass_shatter_count++;
					shatter_timer = 0;
				}

				else
				{
					LoadGame();
				}
			}
		}

		if (menuWindows [menuIndex].levelsScrollBar != null) {
			for (int i = 0; i < levelButtons.Count; i++) {
				if (levelButtons [i] != null) {
					//get the button position normalized from 0 to 1;
					float normalizedButtonPos = (float)i / (levelButtons.Count - 1);

					//get the normalized value of normalizedButtonPos as it approximates menuWindows [menuIndex].levelsScrollBar.value
					//if menuWindows [menuIndex].levelsScrollBar.value is one how close is normalizedButtonPos to it on a scale of 0 to 1.
					//anything more than 0.5f away from levelsScrollBar.val shall be 0.
					float distBetweencurrentAndVal = Mathf.Abs (normalizedButtonPos - menuWindows [menuIndex].levelsScrollBar.value);

					levelButtons [i].eulerAngles =new Vector3 (0, normalizedButtonPos - menuWindows [menuIndex].levelsScrollBar.value, 0) * 50;

					float normalizedSizeIncrease = (1 - (distBetweencurrentAndVal*2f));
					if (normalizedSizeIncrease < 0) {
						normalizedSizeIncrease = 0;
					}

					levelButtons [i].localScale = new Vector3 (normalizedSizeIncrease, normalizedSizeIncrease, 1);
					levelButtons[i].anchoredPosition = new Vector2(levelButtons[i].anchoredPosition.x, -distBetweencurrentAndVal*50);

					//for the organization depending on index.
					levelButtons [i].name = (1-distBetweencurrentAndVal).ToString ();

					Color newcol = new Color (1,1,1, 1-distBetweencurrentAndVal);
					levelButtons[i].GetComponent<Image>().color = newcol;
					GameObject button = levelButtons[i].transform.GetChild(0).gameObject;
					button.GetComponent<Image>().color = newcol;
					LevelButtonScript btnScript = button.GetComponent<LevelButtonScript>();
					if (btnScript.score >= 0)
					{
						btnScript.levelScoreIcon.color = newcol;
					}
					btnScript.LevelScreenShot.color = newcol;
				}
			}

		
			//sort the level buttons by their name...
			Transform[] levelButtonsArray = levelButtons.OrderBy  (go => go.name).ToArray();
			for (int i = 0; i <levelButtonsArray.Length; i++) {
				levelButtonsArray [i].SetSiblingIndex (i);
			}
		}
	}

	
	public void LoadGame ()
	{
		PlayerPrefs.SetInt ("number_Minions", number_Minions);
		Application.LoadLevel (index);
	}

	public void QuitGame()
	{
		Application.Quit ();
	}

	public void SetMenuPos(int index)
	{
		StartCoroutine ("animateAndChangeMenuPos", index);
	}

	public IEnumerator animateAndChangeMenuPos(int index)
	{
		//make the old buttons dissapear
		for (int i = 0; i < menuWindows[menuIndex].elements.Count; i++) 
		{
			Button btn = menuWindows[menuIndex].elements[i].GetComponent<Button>();
			if (btn != null)
			{
				btn.enabled = false;
			}
			menuWindows[menuIndex].elements[i].GetComponent<Animator>().SetTrigger("Dissappear");
			yield return new WaitForSeconds(0.05F);
		}

		//change the menu index
		if (index != menuIndex) {
			previousMenuPos = menuIndex;
		}
		menuIndex = index;


		//animate the new buttons to appear
		for (int i = 0; i < menuWindows[menuIndex].elements.Count; i++) 
		{
			Button btn = menuWindows[menuIndex].elements[i].GetComponent<Button>();
			if (btn != null)
			{
				btn.enabled = true;
			}
			menuWindows[menuIndex].elements[i].GetComponent<Animator>().SetTrigger("Appear");
			yield return new WaitForSeconds(0.075F);
		}

		
		if (menuWindows[menuIndex].levelsScrollBar != null)
		{
			menuWindows[menuIndex].levelsScrollBar.size = 0.1f;
			menuWindows[menuIndex].levelsScrollBar.value = 0;
		}

		yield return null;
	}

	public void SetNoMinions()
	{
		number_Minions = (int)No_Minions_Slider.value;
		numberMinionsText.text = "Number Of Minions: " + number_Minions;
	}

	public void SetToPreviousPos ()
	{
		SetMenuPos (previousMenuPos);
	}

	public void LevelButtonPressed (int i)
	{
		if (explosive != null) {
			explosive.GetComponent<Explodable> ().Explode ();
		}

		SetMenuPos(3);
		index = i;
		levelSelected = true;
	}
}

[System.Serializable]
public class menuWindow
{
	public string name;
	public List<GameObject> elements;
	public RectTransform levelsParent;
	public Scrollbar levelsScrollBar;
}
