﻿using UnityEngine;
using System.Collections;

public class Explodable : MonoBehaviour {

	public GameObject explosionPrefab;
	public float fuseDur = 2;
	float fuseTimer = 0;
	public bool isActive = false;

	public float explosionMagnitude = 10;

	
	// Update is called once per frame
	void Update () {
		if (isActive) {
			fuseTimer += Time.deltaTime;
			if (fuseTimer > fuseDur) {
				Explode();
			}
		}
	}

	public void Explode()
	{
		Instantiate (explosionPrefab, transform.position, transform.rotation);
		Destroy (gameObject);
	}

	void OnCollisionEnter(Collision collision) 
	{
		if (collision.relativeVelocity.magnitude > explosionMagnitude)
		{
			Explode();
		}
	}
}
