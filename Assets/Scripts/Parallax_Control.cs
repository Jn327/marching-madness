﻿using UnityEngine;
using System.Collections;

public class Parallax_Control : MonoBehaviour 
{
	public GameObject follow_camera;
	public float paralax_speed;
	private Vector3 orginal_pos;

	void Start()
	{
		orginal_pos = transform.position;
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.position = new Vector3 (orginal_pos.x - (paralax_speed * follow_camera.transform.position.x), transform.position.y, transform.position.z);
	}
}
