﻿using UnityEngine;
using System.Collections;

public class PlayAudioSourceOnCollision: MonoBehaviour {

	public AudioSource[] audioClips;
	

	void OnCollisionEnter(Collision collision) 
	{
		if (collision != null && audioClips.Length > 0)
		{
			if (collision.relativeVelocity.magnitude > 2)
			{
				int audioIndex = Random.Range(0, audioClips.Length);
				audioClips[audioIndex].Play();
				audioClips[audioIndex].transform.position = collision.contacts[0].point;
			}
		}
	}
}
