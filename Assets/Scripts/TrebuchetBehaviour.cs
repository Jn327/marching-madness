﻿using UnityEngine;
using System.Collections;

public class TrebuchetBehaviour : MonoBehaviour {

	public GameObject projectile;
	public GameObject attackPos;
	float attackTimer = 0.33f;
	public float attackFreq = 2;

	public float forceAmount = 500;

	public GameObject[] destroyPrefabs;
	public Transform[] DestroyTransforms; //where to instantiate the destroyed parts..
	public float destroyForce = 5;
	

	// Update is called once per frame
	void FixedUpdate () 
	{
		attackTimer += Time.deltaTime;
		if (attackTimer >= attackFreq) 
		{
			GetComponent<AudioSource>().Play();
			GameObject project = Instantiate(projectile, attackPos.transform.position, attackPos.transform.rotation) as GameObject;
			project.GetComponent<Rigidbody>().AddForce(attackPos.transform.right * forceAmount*Random.Range(0.75f,1.5f));
			attackTimer = 0;
		}
	}

	void OnCollisionEnter(Collision collision) {
		
		if (collision.relativeVelocity.magnitude > destroyForce) {
			GetDestroyed();
		}
	}

	public void GetDestroyed()
	{
		gameObject.tag = "Untagged"; //otherwise it was sometimes spawning two catapults when hit by tnt...
		for (int i = 0; i < DestroyTransforms.Length; i++)
		{
			Instantiate (destroyPrefabs[i], DestroyTransforms[i].position, DestroyTransforms[i].rotation);
		}
		Destroy (gameObject);
	}
}
